
import java.util.ArrayList;


/**
 * Created by User on 29.09.2017.
 */
public class Ship {
    private static ArrayList<String> location = new ArrayList<>();

    /**
     * Метод для присваивания начального положения коробля.
     *
     * @param с координаты корабля.
     */
    public void setLocation(int с) {
        location.add(Integer.toString(с));
        location.add(Integer.toString(с + 1));
        location.add(Integer.toString(с + 2));
    }

    /**
     * Метод для обработки выстрела пользователя,
     * при выстреле выводит сообщение пользователю "Ранил","Убил." или "Мимо".
     *
     * @param shot координата выстрела ,который принимает значение введеное пользователем с клавиатуры.
     */

    public String shoots(int shot) {
        int index = location.indexOf(Integer.toString(shot));
        String line = "Мимо";
        if (index != -1) {
            location.remove(index);
            if (location.isEmpty()) {
                line = "Убит";
            } else {
                line = "Ранил";
            }
        }
        return line;
    }

    /**
     * Метод toString() выводит  информацию о текущей позиции корабля.
     *
     * @return возвращает положение корабля.
     */

    @Override
    public String toString() {
        return "Расположение коробля = " + location +
                '}';
    }
}

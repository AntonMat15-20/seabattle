import java.util.Scanner;

/**
 * Основной метода для запуска игры морской бой,
 * пользователь вводит одну координату
 * и при попадении выводится соответствующая строка.
 * /**
 * Created by User on 29.09.2017.
 */
public class Game {
    static final int c = (int) (Math.random() * 8);
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Ship ship = new Ship();
        ship.setLocation(c);
        System.out.println(ship.toString());
        int attempt = 0;
        String line;
        do {
            System.out.println("Введите координату ");
            int shot = scanner.nextInt();
            attempt++;
            line = ship.shoots(shot);
            System.out.println(line);
        } while (!line.equals("Убит"));
        System.out.println("Ура!!!Ты победил.");
        System.out.println("Количество попыток = " + attempt);
    }
}


